package com.example.demo.domain;

import com.example.demo.command.EngineCommand;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@Setter
@Getter
@NoArgsConstructor
@Entity(name = "engines")
@Table
public class Engine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false)
    private String cylinders;
    @Column(nullable = false)
    private String power;

    @OneToMany(mappedBy = "engine", cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<Car> car;

    @Column
    private Boolean deleted = false;


    public Engine create(EngineCommand engineCommand) {
        Engine engine = new Engine();
        engine.cylinders = engineCommand.getCylinders();
        engine.power = engineCommand.getPower();
        return engine;
    }

    public  Engine update(EngineCommand engineCommand) {
        this.cylinders = engineCommand.getCylinders();
        this.power = engineCommand.getPower();
        return this;
    }

    public void delete(){
        this.deleted = true;
    }


}
