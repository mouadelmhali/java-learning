package com.example.demo.domain;


import com.example.demo.command.CarCommand;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@Setter
@Getter
@NoArgsConstructor
@Entity(name = "cars")
@Table
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false)
    private String model;
    @Column(nullable = false)
    private String color;

    @ManyToOne
    private Engine engine;

    @Column
    private Boolean deleted = false;


    public Car create(CarCommand carCommand) {
        Car car = new Car();
        car.model = carCommand.getModel();
        car.color = carCommand.getColor();
        return car;
    }

    public  Car update(CarCommand carCommand) {
        this.model = carCommand.getModel();
        this.color = carCommand.getColor();
        return this;
    }

    public void delete(){
        this.deleted = true;
    }
}