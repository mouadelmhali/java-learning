package com.example.demo.controller;

import com.example.demo.command.CarCommand;
import com.example.demo.dto.CarDto;
import com.example.demo.dto.mappers.CarMapper;
import com.example.demo.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/cars")
@RequiredArgsConstructor
public class CarController {

    private final CarService carService;
    private final CarMapper carMapper;

    @GetMapping
    public ResponseEntity<List<CarDto>> getAllCars() {
        return ResponseEntity.ok(carMapper.toListCarDto(carService.getAllCars()) );
    }

    @GetMapping("/{id}")
    public ResponseEntity<CarDto> getCar(@PathVariable Integer id) {
         return ResponseEntity.ok(carMapper.toCarDto(carService.getCar(id)));
    }

    @DeleteMapping("/{id}")
    public void deleteCar(@PathVariable Integer id){
        carService.deleteCar(id);
    }

    @PostMapping
    public void createCar(@RequestBody final CarCommand car) {
        carService.createCar(car);
    }

    @PutMapping("/{id}")
    public void updateCar(@RequestBody final CarCommand car,@PathVariable Integer id) {
        carService.updateCar(car,id);
    }

}
