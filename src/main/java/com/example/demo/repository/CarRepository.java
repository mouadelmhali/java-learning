package com.example.demo.repository;


import com.example.demo.domain.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CarRepository extends JpaRepository<Car, Integer> {
    @Query(value = "SELECT * FROM cars WHERE deleted = false",nativeQuery = true)
    List<Car> findAllByDeletedFalse();

    @Query("SELECT p from cars p where p.id in :id and p.deleted = false")
    Optional<Car> findByIdAndDeletedFalse(Integer id);

}