package com.example.demo.command;

import lombok.*;

@Getter
@RequiredArgsConstructor
public class CarCommand {
    private String model;
    private String color;
}