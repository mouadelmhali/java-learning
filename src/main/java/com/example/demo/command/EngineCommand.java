package com.example.demo.command;


import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class EngineCommand {
    private String cylinders;
    private String power;
}
