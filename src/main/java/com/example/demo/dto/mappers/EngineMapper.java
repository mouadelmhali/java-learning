package com.example.demo.dto.mappers;

import com.example.demo.domain.Car;
import com.example.demo.domain.Engine;
import com.example.demo.dto.EngineDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EngineMapper {
    EngineDto toEngineDto (Engine engine);

    List<EngineDto> toListEngineDto (List<Engine> engine);
}
