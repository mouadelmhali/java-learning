package com.example.demo.dto.mappers;

import com.example.demo.domain.Car;
import com.example.demo.dto.CarDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring",uses = EngineMapper.class)
public interface CarMapper {
    CarDto toCarDto(Car car);

    List<CarDto> toListCarDto(List<Car> car);
}
