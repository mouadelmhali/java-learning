package com.example.demo.dto;


import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class CarDto {
    private int id;
    private String model;
    private String color;
    private EngineDto engine;
}

