package com.example.demo.service;

import com.example.demo.command.CarCommand;
import com.example.demo.domain.Car;
import com.example.demo.repository.CarRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class CarServiceImp implements CarService{

    private final CarRepository carRepository;

    @Override
    public List<Car> getAllCars() {
        return carRepository.findAllByDeletedFalse();
    }

    @Override
    public Car getCar(Integer id) {
        return carRepository.findByIdAndDeletedFalse(id).orElseThrow(RuntimeException::new);
    }

    @Override
    public void deleteCar(Integer id) {
        Car car = carRepository.findById(id).orElse(null);
        car.delete();
        carRepository.save(car);
    }

    @Override
    public void updateCar(CarCommand carCommand,Integer id) {
        Car car = carRepository.getOne(id);
        car.update(carCommand);
        carRepository.save(car);
    }

    @Override
    public void createCar(CarCommand carCommand) {
        Car car = new Car();
        carRepository.save(car.create(carCommand));
    }
}
