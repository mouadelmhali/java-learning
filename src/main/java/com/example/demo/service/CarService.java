package com.example.demo.service;

import com.example.demo.command.CarCommand;
import com.example.demo.domain.Car;

import java.util.List;
import java.util.Optional;

public interface CarService {
    List<Car> getAllCars();

    Car getCar(Integer id);

    void deleteCar(Integer id);

    void updateCar(CarCommand carCommand,Integer id);

    void createCar(CarCommand carCommand);
}
